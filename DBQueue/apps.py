from django.apps import AppConfig


class DbqueueConfig(AppConfig):
    name = 'DBQueue'
