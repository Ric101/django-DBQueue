from django.dispatch import receiver
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.db import models


# TAG
class TagManager(models.Manager):
    def get_by_natural_key(self, tipo, tag):
        return self.get(tipo=tipo, tag=tag)


class Tag(models.Model):
    """
    Tag a 2 livelli, con ForeignKey generica perché possa attaccarsi dappetutto
    ATTENZIONE! Chiunqua voglia poter essere taggato, deve avere in campo
    tags = generic.GenericRelation('Tag', content_type_field='taggato_type', object_id_field='taggato_id')
    NB: == redefinito!! desc non conta, per essere uguali, 2 tag devono avere stringhe uguali in tag e tipo!
            per essere COMPATIBILI devono avere lo stesso tipo, e almenu 1 dei 2 il tag == None
    """
    objects = TagManager()
    #taggato -> può essere null, ad es se aggiungo dei tag nel modello che li contiene (anziché appenderli usando la fk generica. questo può essere utile se ad es un modello ha bisogno di diversi "gruppi" di tag, posso fare più ManyToMany!!)
    taggato_type = models.ForeignKey(ContentType, null=True)
    taggato_id = models.PositiveIntegerField(null=True)
    taggato = GenericForeignKey('taggato_type', 'taggato_id')   #elemento cui viene applicato il tag

    tag = models.CharField(max_length=69)
    tag_int = models.IntegerField(null=True)   #nel caso voglia che il tag sia un intero!
    tipo = models.CharField(max_length=69, default='')
    desc = models.CharField(max_length=255, null=True)

    def __eq__(self, other):
        """
        override di == Ritorna True se sia i campi 'tag' che tipo sono uguali, ritorna
        None se sono COMPATIBILI (cioè se uno dei 2 non ha il tag, ma slo il tipo, ed i 2 tipi sono uguali),
        altrimenti ritorna False. NB: il non avere un tipo, invece NON è compatibile con qualunque tipo!!
        other può essere sia un'istanza di Tag, che un dict {'tag':'xxx', 'tag_tipo':'xxx'},
        che una semplice string (in questo caso si assume tipo = ''
        """
        try:
            other = {'tag': other.tag, 'tag_tipo': other.tipo}
        except:
            if is_stringa(other):
                other = {'tag': other, 'tag_tipo': ''}

        if self.tag==other['tag'] and self.tipo==other['tag_tipo']:
            return True
        elif (self.tipo==other['tag_tipo']) and (not self.tag or not other['tag']):
            return None
        else:
            return False
    def __str__(self):
        return str(self.tag) + "; " + str(self.tipo) + "; " + str(self.tag_int)
    def __unicode__(self):
        return str(self)
    def in_taglist(self, taglist):
        """
        taglist è un queryset di Tag, o una list di {'tag':'xxx', 'tag_tipo':'xxx'}, o di stringhe
        Ritorna:    True se il tag(self) è contenuto in taglist,
                    None se taglist ha almeno un elemento compatibile con Tag
                            (stesso tipo, ma almeno uno dei 2, di solito l'elemento di taglist, ha tag == None o ''),
                    False altrimenti
        """
        compatibile = False
        for tag in taglist:
            if self==tag:
                return True
            if (self==tag) is None:
                compatibile = True

        if compatibile:
            return None

        return False

    @staticmethod
    def tag_in_taglist(tag, taglist):
        """
        tag può essere una stringa, un dict {'tag':'xxx', 'tag_tipo':'xxx'}, oppure
        un'istanza di Tag
        """
        try:
            tag.tag
        except:
            if is_stringa(tag):
                tag = {'tag': tag, 'tag_tipo': ''}
            tag = Tag(tag=tag['tag'], tipo=tag['tag_tipo'])

        return tag.in_taglist(taglist)

    @staticmethod
    def taglist_in_taglist(contenuta, contenente):
        """
        entrambe le taglist devono avere la forma di dict: {'tag':'xxx', 'tag_tipo':'xxx'}
        ritorna     True se e solo se contenente contiene tutti e tag elencati in contenuta!
                    None se almeno uno dei tag non è contuno in form identica, ma in forma "compatibile"
                        (generalmente perché il contenente ha dei tag che richiedono solo il tipo (campo tag vuoto)
                    False altrimenti
        """
        solo_compatibile = False
        for tag in contenuta:
            tipo_contenimento = Tag.tag_in_taglist(tag, contenente) #True, False o None!!
            if tipo_contenimento is False:
                return False
            elif tipo_contenimento is None:
                solo_compatibile = True

        if solo_compatibile:
            return None

        return True

    @staticmethod
    def filter_and(queryset, tags, tieni_compatibili=True):
        """
        queryset è un QuerySet di elementi potenzialmente taggati,
        ritorna un (nuovo) queryset contenente il sottoinsieme di elementi
        che rispettano tutti (AND) i tag richiesti da tags(ovviamente con anche il tipo, se c'è)
        tags può essere un tag semplice (stringa) oppure un dict semplice {'tag':'xxx', 'tag_tipo':'xxx'}
        oppure una list di stringhe o dict, oppure un queryset di Tag!

        tieni_compatibili serve a discriminare se gli elementi taggati in modo solo "compatibile" debbano essere
        scartati o meno. default a True -> nel caso classico, in tags posso filtrare solo per tipo (mettendo il
        campo "tag" a '' o None)
        """
        if is_stringa(tags) or is_dict(tags):
            tags = [tags]

        remove_list = []
        for el in queryset:
            tags_el = el.tags.all()

            tipo_contenimento = Tag.taglist_in_taglist(tags, tags_el) #Ture, False o None
            if tipo_contenimento is False or (tipo_contenimento is None and not tieni_compatibili):
                remove_list.append(el.id)

        return queryset.exclude(id__in=remove_list)


# MetaData
class MetaData(models.Model):
    """
    un generico valore che permette di fungere da modificatore per le varie
    caratteristiche/classi o qualunque cosa lo richieda.
    Es ogni classe può avere un modificatore che le "personalizza"
    il tempo di avanzamento.

    - proprietario è l'elemento che hai dei modificatori associati (Es Fighter, Fighter_caratteristica, ...)
    - modificato è l'elemento modificato vero e prorio, cioè il cui valore subisce la modifica. ad es se
      proprietario Fighter_caratteristica, modificato può essere la moneta "cash", e quindi il modificatore
      modificherà il costo in chash di miglioramento della caratteristica e fighter associati al
      Fighter_caratteristica proprietario

      .modificato_str rimpiazza la GenericForeignKey nei casi in cui il modificato non sia un'elemento del
      db. Ad es potrebbe essere il "tempo", inteso ad es come tempo richiesto per eseguire una mossa,
      per cui un fighter potrebbe richiedere un modificatore. Più in generale, si potrebbe avere un modificato
      chiamato "tempo apprendimento mosse di sottomissione", sarà poi responsabilità di
      fighter.IMP.costo_miglioramento_mossa() andare a cercare per il modificatore "tempo apprendimento mosse di
      sottomissione" ogni qualvolta calcoli il tempo di miglioramente per una mossa taggata con tipo "sottomissione"
               oppure
      .modificato_str DEFINISCE la generic Foreign key. es se una caratteristica costa euri, e produce euri (un po' stupido
      ma teoricamente possibili) potrei avere 2 modificatori per lo stesso peroprietario e il cui modificato è la
      stessa caratteristica, me con modificato_str diverso, perché modificano 2 "azioni" diverse!

    NB: un modificatore è NUMERICO, NON fa il lavoro dei tag, serve ad aggiungere personalizzazioni a valori che varranno calcolati (es tempo di aumento livello di una abilità, se voglio che abilità diverse abbiano tempi che aumentano con il livello seguendo regole/proporzionalità diverse diverse)

    ADDENDUM: il Modificatore è reso ancora più potente!!
        - ha anche un valore_txt e un valore_orario(da usare con o in alternativa al valore numerico)
        - può essere usato come tag + valore (es usato in code, un Coda_el può avere associati
          valori aggiuntivi che non sono propri dell'elemnto messo in coda, ma che sono funzionali
          alla gestione della coda, o alla gestione dell'elemento in coda, mentre è in conda!) (usato, ad es da Fighter_mosse, per memorizzare il numero di esecuzioni comprate per ogni presenza in coda di una determinata mossa)
        - ha un nome per rimpiazzare il tag, quando questo sia una semplice stringa (risparmia unachiamata al db)
    """

    proprietario_type = models.ForeignKey(ContentType, related_name='tipo_proprietario_modificatore', null=True)
    proprietario_id = models.PositiveIntegerField(null=True)
    proprietario = GenericForeignKey('proprietario_type',
                                             'proprietario_id')  # elemento cui viene applicato il modificatore

    modificato_type = models.ForeignKey(ContentType, null=True)
    modificato_id = models.PositiveIntegerField(null=True)
    modificato = GenericForeignKey('modificato_type',
                                           'modificato_id')  # elemento cui viene applicato il modificatore
    modificato_str = models.CharField(max_length=255, null=True)

    tag = models.ForeignKey('Tag', null=True)
    tipo = models.CharField(max_length=69, null=True)
    nome = models.CharField(max_length=69, null=True)
    meta = models.TextField(null=True)
    desc = models.ForeignKey('Testo',
                             null=True)  # posso aggiungere una descrizione tradotta per spiegare agli utenti il significato del valore!
    orario = models.DateTimeField(null=True)

    tipo_val = models.CharField(max_length=69)

    val_int = models.IntegerField(null=True)
    val_float = models.FloatField(null=True)
    val_str = models.TextField(null=True)  # usato anche per tipo_val == "json_dict"
    val_orario = models.DateTimeField(null=True)

    # il modificatore può essere applicato solo per un certo lasso temporale
    validita_inizio = models.DateTimeField(null=True)
    validita_fine = models.DateTimeField(null=True)

    def serializzabile(self):
        meta = self.meta
        if not meta:
            meta = {}
        else:
            meta = json.loads(meta)
            if not meta:
                # in alcuni casi, es quando ho preso dei valori dal client,
                # self. meta può essere la stringa 'null', ml qual caso mi assicuro
                # che diventi un dict vuoto!
                meta = {}

        return {
            "id": self.id,
            "tipo": self.tipo,
            "tipo_val": self.tipo_val,
            "nome": self.nome,
            "meta": meta,
            "desc": self.desc_id,
            "val": self.val(serializzabile=True),
        }

    def val(self, serializzabile=False):
        if self.tipo_val not in ("orario", "json_dict"):
            return getattr(self, "val_" + self.tipo_val)
        elif self.tipo_val == "orario":
            if serializzabile:
                return da_datetime_a_ISO(self.val_orario)
            else:
                return self.val_orario
        elif self.tipo_val == "json_dict":
            return json.loads(self.val_str)

    def val_uguale_a_(self, x):
        """
        NB: al momento funziona SOLO per val di tipo numerico o stringhe NON json

        ritorna true se x rapprsenta lo stesso valore di self.val()
        """
        if self.tipo_val in ("int", "float"):
            try:
                x = float(x)
            except TypeError:
                return False

            return self.val() == x  # x potrebbe arrivere come stringa

        if self.tipo_val == "str":
            return unicode(self.val()) == unicode(x)

        if self.tipo_val == "orario":
            val = self.val()
            if val is None:
                return x is None

            tipo = type(val)
            if type(x) != tipo:
                if type(x) in (str, unicode):
                    try:
                        return val == da_ISO_a_datetime(x)
                    except TypeError:
                        return False

                try:
                    return val == tipo(x)
                except ValueError:
                    return False

    def clona(self):
        ret = self
        ret.id = None
        ret.save()
        return ret

    @transaction.atomic
    def set_val(self, val, tipo_val=None):
        """
        ATTENZIONE: se tipo_val=None, almeno un valore deve essere già presente,
         e, val, che andrà a sostituirlo ,deve essere dello stesso tipo,
         altrimenti tipo_val determini il tipo del nuovo valore, e se ce n'era già
         uno, anche di un altro tipo, viene eliminato!!

         ritorna True se ho impastato correttamente, False in caso di errore

         NB: tipo_val == "json_dict" -> passo val come dict, lo jsonnizzo, qui!
        """
        self.val_int = None
        self.val_float = None
        self.val_str = None
        self.val_orario = None

        if tipo_val is None:
            tipo_val = self.tipo_val

        if tipo_val is not None:
            self.tipo_val = tipo_val

        if tipo_val == "json_dict":
            self.val_str = json.dumps(val)
        elif tipo_val == "orario" and type(val) in (str, unicode):
            self.val_orario = da_ISO_a_datetime(val)
        else:
            setattr(self, "val_" + self.tipo_val, val)

        self.save()
        return True


# QUEUE
class QueueItem(models.Model):
    """
    elemento_in_coda, deve avere un id numerico (cioè normale)
    ad ogni queue_item è possibile aggiungere dei metadati (meta) che sono implementati usando
    il Modificatore (definito qui sotto). I metadati hanno un name (stringa) che li identifica
    (più metadati con lo stesso name.. possibili ma attenzione!!) ed un valore a scelta
    fra i campi dicoponibili in Modificatore (proprietario viene usato per collegare il Modificatore al QueueItem)
    Seppur disponibili su db, i metadati, per gli utenti della Queue, NON sono accessibili con le normali
    classi di Django, am sottoforma di dict (dove le keys corrispondono al name del Modificatore) e i values
    a oggetti Modificatore. Tutto sto casino è necesasrio perchè usando una normale Generic relation,
    i metadati andrebber opersi quando si ESTRAE un item dalla queue (chiamando queue_item.delete() )

    NB: I Meta possono essre impostati/modificati nel dict, poi verrano salvati sul div (save() sovrascritto)
       perchè SONO COMUMQUE istanze di Modificatore! Al momento invece NON c'è nessun metodo per aggiungere
       o togliere dei meta specifici.. si creato TUTTI alla creazione del QueueItem (Queue.insert_item())
    """
    queue = models.ForeignKey('Queue')
    time = models.DateTimeField(null=True)
    pos = models.IntegerField()
    
    item_type = models.ForeignKey(ContentType, null=True)
    item_id = models.PositiveIntegerField(null=True)
    item = GenericForeignKey('item_type', 'item_id')
    item_txt = models.TextField(null=True) #item in queue può essere un item del db, oppure una stringa di max 255 caratteri!

    def __str__(self):
        if self.item_txt:
            tmp = "(pos {0}) txt -> {1}".format(self.pos, self.item_txt)
            return tmp
        else:
            tmp = "(pos {0}) item -> {1}".format(self.pos, self.item_type.get_object_for_this_type(id=self.item_id))
            return tmp

    def save(self, *args, **kwargs):
        try:
            for m in self.meta:
                self.meta[m].save()
        except:
            pass

        super(QueueItem, self).save(*args, **kwargs)  # Call the "real" save() method.

    @staticmethod
    @receiver(post_delete, sender='sottomarino.QueueItem')
    def _post_delete(sender, instance, **kwargs):
        instance.elimina_meta_da_db()

    def serializzabile(self):
        try:
            return self.item.serializzabile()
        except:
            return self.__unicode__()

    def leggi_meta(self):
        self.meta = {}
        self_type = ContentType.objects.get_for_model(self)
        metas = Modificatore.objects.filter(   # QuerySet con tutti i Modificatore di questo queue_item
            proprietario_type__pk = self_type.id,
            proprietario_id = self.id
        )
        for meta in metas:
            self.meta[meta.name] = meta

    def elimina_meta_da_db(self):
        self_type = ContentType.objects.get_for_model(self)
        metas = Modificatore.objects.filter(   # QuerySet con tutti i Modificatore di questo queue_item
            proprietario_type__pk = self_type.id,
            proprietario_id = self.id
        )
        for meta in metas:
            meta.delete()


class Queue(models.Model):

    # type, name: not required, but can be useful when managing multiple queues
    type = models.CharField(max_length=69, null=True)
    name = models.CharField(max_length=69, null=True)

    # GenericRelation ensures tags get removed if their Queue is
    tags = GenericRelation('Tag', content_type_field='taggato_type', object_id_field='taggato_id')

    # max numbers of items the queue can contain. <=0: queue disabled, None: unlimited
    max_size = models.IntegerField(null=True)

    # Gestione statica delle code
    @classmethod
    def _formatta_tag(cls, tag):
        """
        tag può essere una stringa semplice oppure un oggetto o un dict, che contengano
        un item chiamato tag, e facoltativamente, il corrispondente tag_type

        Ritorna il tag formattato come dict: {'tag':'xxx', 'tag_type':None|''|'yyy'} (None se usa
        per indicare un valore qualunque, ad es nelle ricerche, è != da '')
        """
        if type(tag) is str:
            return {'tag': tag, 'tag_type': None}

        elif type(tag) is dict:
            try:
                _tag = tag['tag']
            except KeyError:
                _tag = None

            try:
                _tag_type = tag['tag_type']
            except KeyError:
                _tag_type = None

        else:
            try:
                _tag = tag.tag
            except AttributeError:
                _tag = None

            try:
                _tag_type = tag.tag_type
            except AttributeError:
                _tag_type = None

        return {'tag': _tag, 'tag_type': _tag_type}

    @classmethod
    def trova_code(cls, proprietario=None, tags=[]):
        """
        ritorna una LIST di Queue, che soddisfa tutti i requisiti passati (tutti facoltativi)
        tags può essere un tag semplice (stringa) oppure un dict semplice {'tag':'xxx', 'tag_type':'xxx'}
        oppure una list di stringhe o dict, oppure un queryset di Tag!
        """
        if type(tags) not in (list, tuple):
            tags = [tags]

        if proprietario:
            proprietario_type = ContentType.objects.get(app_label="sottomarino", model=proprietario.__class__.__name__)
            code = Queue.objects.filter(proprietario_id=proprietario.id, proprietario_type=proprietario_type)

        else:
            code = Queue.objects.all()

        code = code.distinct() #rimuove i duplicati, nel caso ve ne siano
        code = Tag.filter_and(code, tags)
        return code

    # Managemnt of elements in the Queue
    def _find_item(self, element=None):
        """
        Searching for an element (element) in the Queue
        """
        
        # element might be None: returns the sorted queryset of all QueueItem-s in the list
        if element is None:  
            items = QueueItem.objects.filter(queue=self).order_by('pos')
            for element in items:
                element.leggi_meta()
            return items

        # element might be an int: return the element-th QueueItem in the Queue)
        if type(element) is int:
            try:
                queue_item = QueueItem.objects.get(queue=self, pos=element)
            except QueueItem.DoesNotExist:
                queue_item = None
        
        # element might be a string: return the QueueItem storing 'element' as a string value 
        elif type(element) is str:
            try:
                queue_item = QueueItem.objects.filter(queue=self, item_txt=element).order_by('pos')[0]
            except IndexError:
                queue_item = None

        # element might be a Django model instance: return the QueueItem whose value is a (generic) foreignkey to it
        else:
            item_type = ContentType.objects.get_for_model(element) 
            try:
                queue_item = QueueItem.objects.filter(queue=self, item_id=element.id, item_type=item_type).order_by('pos')[0]
            except IndexError:
                queue_item = None

        try:
            queue_item.leggi_meta()
        except (NameError, AttributeError):
            queue_item = None

        return queue_item

    @transaction.atomic
    def insert_element(self, element, pos=None, chronologically=False, time=None, metas=None):
        """
        @:param pos: position at which item should be inserted. 1: at the beginning, None: at the end 
        @:param chronologically: if True: parameter 'pos' must be None, item is placed   
        
        , si aspetta che item abbia un item 'time' (oppure v. sotto),
        e l'inserimento viene fatto per time (item iniziale della queue, il più nel passato,
        finale il più nel futuro, a parità di time, FIFO (l'ultimo inserito, è inserito dopo qutti quelli
        con il suo stesso time)

        time: se item è una stringa (o cmq non non ha un time) posso dargli io un time
                (obblicatorio se ceronologicamente=True, altrimenti facoltativo), che
                diventerà (eventualmente rimpiazzando quello vecchio) il valore time dell'item
                (QueueItem.time)!
        """
        
        items = QueueItem.objects.filter(queue=self).order_by('pos')
        n_items = items.count()
        
        # reject insertion if Queue is full
        if self.max_size is not None and n_items >= self.max_size:
            return False
        
        if chronologically:
            # inserting item after the last item the time of which is <= the insertion time
            # will rise exception if no time is available
            if not time:
                time = element.time
                
            pos = QueueItem.objects.filter(queue=self, time__lte=time).count() + 1

        else:
            # insert item in desired position
            if pos is None or pos > n_items:
                pos = n_items + 1

            elif pos < 1:
                pos = 1

        if type(element) is str:
            queue_item = QueueItem(queue=self, item_txt=element, pos=pos)
        else:
            queue_item = QueueItem(queue=self, item=element, pos=pos)

        if time:
            queue_item.time = time

        items = items[pos-1:]     # Queue lists items using 1-base

        for item in items:
            item.pos += 1
            item.save()
            
        queue_item.save()

        if metas:
            if type(metas) not in (list, tuple):
                metas = [metas]
                
            for meta in metas:
                meta.proprietario = queue_item
                meta.save()
                
        return pos

    @transaction.atomic
    def extract_item(self, element=1):
        """
        :param element: element position in the Queue, or an element of the Queue (string or Django model instance)
        :returns: the chosen Item (which gets also removed from the Queue). None if it could not find it.
        """
        queue_item = self._find_item(element) if element is not None else None

        if queue_item is not None:
            pos = queue_item.pos
            queue_item.delete()
            items = QueueItem.objects.filter(queue=self, pos__gt=pos)
            for item in items:
                item.pos -= 1
                item.save()

        return queue_item

    def get_item(self, element=None):
        """
        :param element: element position in the Queue, or an element of the Queue (string or Django model instance)
        :return: QueueItem Instance, or QuerySet of all instances
        """
        queue_item = self._find_item(element) if element is not None else None
        return queue_item

    def find_item(self, element):
        """
        :param element: element position in the Queue, or an element of the Queue (string or Django model instance)
        :return: instance of QueueItem or None
        """
        queue_item = self._find_item(element) if element is not None else None
        if queue_item:
            return queue_item.pos
        return None

    # Gestione Tag
    def _trova_tag(self, tag):
        #tag deve essere un dict, con 0 o più attributi fra: 'tag' e 'tag_type'
        _tag = Queue._formatta_tag(tag)
        tag = _tag['tag']
        tag_type = _tag['tag_type']

        contenttype_coda = ContentType.objects.get_for_model(Queue)
        if tag is not None and tag_type is not None:
            tags = Tag.objects.filter(taggato_type=contenttype_coda, taggato_id=self.id, tag=tag, type=tag_type)
        elif tag is None and tag_type is not None:
            tags = Tag.objects.filter(taggato_type=contenttype_coda, taggato_id=self.id, type=tag_type)
        elif tag_type is None and tag is not None:
            tags = Tag.objects.filter(taggato_type=contenttype_coda, taggato_id=self.id, tag=tag)
        else:
            tags = Tag.objects.filter(taggato_type=contenttype_coda, taggato_id=self.id)
        return tags

    @transaction.atomic
    def aggiungi_tag(self, tag):
        """
         se il tag esiste già lo lascia invariato
        """
        _tag = Queue._formatta_tag(tag)
        tag = _tag['tag']
        tag_type = _tag['tag_type']
        if tag_type is None:
            tag_type = ''

        contenttype_coda = ContentType.objects.get_for_model(Queue)
        tag_esistenti = Tag.objects.filter(taggato_type=contenttype_coda, taggato_id=self.id, tag=tag, type=tag_type)
        if not tag_esistenti.count():
            tag = Tag(taggato=self, tag=tag, type=tag_type)
            tag.save()

    @transaction.atomic
    def rimuovi_tag(self, tag):
        """
        Rimuove tutti i tag di questa queue che corrispondono ai criteri passati come
        parametri, lasciare il None significa "tutti" (es tutti quelli con un verto type di tag,
        lascio tag=None, e definisco solo tag_type.
        Ritorna il numero di tag cancellati
        """
        _tag = Queue._formatta_tag(tag)
        tag = _tag['tag']
        tag_type = _tag['tag_type']

        tags = self._trova_tag({'tag':tag, 'tag_type':tag_type})
        i = 0
        for tag in tags:
            tag.delete()
            i += 1
        return i

    def ha_tag(self, tag):
        #ritorna il numero di tag che soddisfano la selezione tag/tag_type
        _tag = Queue._formatta_tag(tag)
        tag = _tag['tag']
        tag_type = _tag['tag_type']

        tags = self._trova_tag({'tag':tag, 'tag_type':tag_type})
        return tags.count()

    def leggi_tags(self, tag={}):
        """
        ritorna un [] di dict, ordine non definito, ammessi anche entrambi None, per TUTTI i tag di questa queue
        formato: [{'tag': 'xxx', 'type': 'yyy'}, ...]
        """
        _tag = Queue._formatta_tag(tag)
        tag = _tag['tag']
        tag_type = _tag['tag_type']

        tags = self._trova_tag({'tag':tag, 'tag_type':tag_type})
        ret = []
        for tag in tags:
            ret.append({'tag':tag.tag, 'tag_type':tag.type})
        return ret

    # Getione Queue
    @transaction.atomic
    def empty_queue(self):
        QueueItem.objects.filter(queue=self).delete()

    @transaction.atomic
    def set_max_size(self, size):
        """
        Note: if setting to number smaller than the current size, all items will remain
        (but it won't be possible to add items until the size decreases below the new max_size)
        """
        self.max_size = size
        self.save()

    def __str__(self):
        items = QueueItem.objects.filter(queue=self).order_by('pos')
        txt = "QUEUE -> id: {0}; elements: {1} \n".format(self.id, items.count())
        for item in items:
            txt += "    {0}\n".format(item)
        return txt
